/* Copyright (C) alfaDesigns - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Suhaib Alfageeh <suhaib@alfadesigned.com>, 2014-2016
 */
 
#import "PropertyFile.h"
#import "substrate.h"

BOOL otherRepo = NO;
/* ***************************************************
Project: StreamPlay
File: StreamPlay.xm
By: Suhaib Alfaqeeh
Copyright 2014-2015, alfaDesigns - All rights reserved.
/****************************************************
/*
=======================
BEGIN Method Manipulation
=======================
*/



 /****************************************************
// Feature: YTBackgroundAudio
//Keep listening to your YouTube videos with the app in the background. Perfect for music. 
/****************************************************/
%hook MLSingleStreamSelector
-(BOOL)backgroundPlaybackAllowed { %log;  if (Ignition && YTBackgroundAudio){ 
BOOL  r = TRUE; NSLog(@" backgroundPlaybackAllowed :  = %hhd", r); return r; }
else{ return %orig; } }

%end

%hook MLPlayerConfig
// +(id)transitionalPlayerConfigFromBackgroundPlaybackConfig:(id)arg1 {
	
// }
%end
	
%hook MLAVPlayer
-(void)setBackgroundPlaybackAllowed:(BOOL)arg1 { %log; if (Ignition && YTBackgroundAudio){ 
arg1 = TRUE; NSLog(@" setBackgroundPlaybackAllowed :  = %hhd", arg1); %orig(TRUE); }
else{ %orig; }}

-(BOOL)backgroundPlaybackAllowed { %log;  if (Ignition && YTBackgroundAudio){ 
BOOL  r = TRUE; NSLog(@" backgroundPlaybackAllowed :  = %hhd", r); return r; }
else{ return %orig; } }

-(BOOL)backgroundPlayback { %log;  if (Ignition && YTBackgroundAudio){ 
BOOL  r = TRUE; NSLog(@" backgroundPlaybackAllowed :  = %hhd", r); return r; }
else{ return %orig; } }

%end
/* %hook GTMHTTPFetcher
-(BOOL)shouldFetchInBackground { %log;  if (Ignition && YTBackgroundAudio){ BOOL  r = TRUE; NSLog(@" isInBackground :  = %hhd", r); return r; }
else{ return %orig; } }
%end
%hook GCK_GCDAsyncSocket
-(BOOL)enableBackgroundingOnSocketWithCaveat:(bool)arg1 { %log;  if (Ignition && YTBackgroundAudio){ BOOL  r = TRUE; NSLog(@" isInBackground :  = %hhd", r); return %orig(r); }
else{ return %orig; } }
%end
%hook MLPlayer

-(BOOL)backgroundPlaybackAllowed { %log;  if (Ignition && YTBackgroundAudio){ 
BOOL  r = TRUE; NSLog(@" backgroundPlaybackAllowed :  = %hhd", r); return r; }
else{ return %orig; } }

-(void)setBackgroundPlaybackAllowed:(BOOL)arg1 { %log; if (Ignition && YTBackgroundAudio){ 
arg1 = TRUE; NSLog(@" setBackgroundPlaybackAllowed :  = %hhd", arg1); %orig(TRUE); }
else{ %orig; }}

%end // MLPlayer

%hook YTPlayerStatus
-(BOOL)backgroundPlayback { %log;  if (Ignition && YTBackgroundAudio){ BOOL  r = TRUE; NSLog(@" backgroundPlayback :  = %hhd", r); return r; }
else{ return %orig; } }
%end // YTPlayerStatus */
 /****************************************************
// Feature: YTHighQuality
/****************************************************/
%hook YTReachability
-(BOOL)isOnWiFi { %log;  if (Ignition && YTHighQuality){ BOOL  r = TRUE; NSLog(@" isOnWiFi :  = %hhd", r); return r; }
else{ return %orig; } }
%end

%hook YTUserDefaults
-(void)setStreamHDOnWiFiOnly:(BOOL)arg1 { %log; if (Ignition && YTHighQuality){ arg1 = FALSE; NSLog(@" setStreamHDOnWiFiOnly :  = %hhd", arg1); %orig(FALSE); }
else{ %orig; }}

-(BOOL)streamHDOnWiFiOnly { %log;  if (Ignition && YTHighQuality){ BOOL  r = FALSE; NSLog(@" streamHDOnWiFiOnly :  = %hhd", r); return r; }
else{ return %orig; } }

-(BOOL)isAdultContentConfirmed { %log;  if (Ignition && YTNoAgeLimit){ BOOL  r = TRUE; NSLog(@" streamHDOnWiFiOnly :  = %hhd", r); return r; }
else{ return %orig; } }
%end

%hook YTSettingsViewController
-(BOOL)setStreamHDOnWiFiOnly:(BOOL)arg1 { %log; if (Ignition && YTHighQuality){ arg1 = FALSE; NSLog(@" setStreamHDOnWiFiOnly :  = %hhd", arg1); %orig(FALSE); return FALSE; }
else{ %orig; return %orig; }}

-(BOOL)onStreamHDOnWiFiOnly:(BOOL)arg1 { %log; if (Ignition && YTHighQuality){ arg1 = FALSE; NSLog(@" onStreamHDOnWiFiOnly :  = %hhd", arg1); return %orig(FALSE); }
else{ %orig; return %orig; }}
%end

%hook YTSettings
-(void)setStreamHDOnWiFiOnly:(BOOL)arg1 { %log; if (Ignition && YTHighQuality){ arg1 = FALSE; NSLog(@" setStreamHDOnWiFiOnly :  = %hhd", arg1); %orig(FALSE); }
else{ %orig; }}

-(BOOL)streamHDOnWiFiOnly { %log;  if (Ignition && YTHighQuality){ BOOL  r = FALSE; NSLog(@" streamHDOnWiFiOnly :  = %hhd", r); return r; }
else{ return %orig; } }
 /****************************************************
// Feature: YTNoAgeLimit
/****************************************************/
-(BOOL)isAdultContentConfirmed { %log;  if (Ignition && YTNoAgeLimit){ BOOL  r = TRUE; NSLog(@" isAdultContentConfirmed :  = %hhd", r); return r; }
else{ return %orig; } }

-(void)setAdultContentConfirmed:(BOOL)arg1 { %log; if (Ignition && YTNoAgeLimit){ arg1 = TRUE; NSLog(@" setAdultContentConfirmed :  = %hhd", arg1); %orig(FALSE); }
else{ %orig; }}
%end
%hook YTApiaryRequestFactory
-(BOOL)adultContentConfirmed { %log;  if (Ignition && YTNoAgeLimit){ BOOL  r = TRUE; NSLog(@" isAdultContentConfirmed :  = %hhd", r); return r; }
else{ return %orig; } }
%end

%hook YTPlayerRequestFactory
-(bool)adultContentConfirmed { %log;  if (Ignition && YTNoAgeLimit){ BOOL  r = TRUE; NSLog(@" isAdultContentConfirmed :  = %hhd", r); return r; }
else{ return %orig; } }
%end

%hook YTVideo

-(BOOL)isAdultContent { %log;  if (Ignition && YTNoAgeLimit){ BOOL  r = FALSE; NSLog(@" isAdultContent :  = %hhd", r); return r; }
else{ return %orig; } }

 /****************************************************
// Feature: YTRemovePrivateContent
/****************************************************/
-(BOOL)isPrivateContent { %log;  if (Ignition && YTRemovePrivateContent){ BOOL  r = FALSE; NSLog(@" isPrivateContent :  = %hhd", r); return r; }
else{ return %orig; } }
 /****************************************************
// Feature: YTNoAds (Cntnd)
/****************************************************/

-(BOOL)isPaidContent { %log;  if (Ignition && YTNoAds){ BOOL  r = FALSE; NSLog(@" isMonetized :  = %hhd", r); return r; }
else{ return %orig; } }


 /****************************************************
// Feature: YTAllowComments
/****************************************************/
-(id)initWithID:(id)arg1 title:(id)arg2 description:(id)arg3 uploaderDisplayName:(id)arg4 uploaderChannelID:(id)arg5 uploadedDate:(id)arg6 publishedDate:(id)arg7 duration:(unsigned int)arg8 viewCount:(unsigned long long)arg9 likeCount:(unsigned long long)arg10 dislikeCount:(unsigned long long)arg11 likesAllowed:(BOOL)arg12 state:(id)arg13 thumbnailURLs:(id)arg14 commentsAllowed:(BOOL)arg15 commentsURL:(id)arg16 commentsCountHint:(unsigned long long)arg17 relatedURL:(id)arg18 claimed:(BOOL)arg19 listed:(BOOL)arg20 categoryLabel:(id)arg21 categoryTerm:(id)arg22 adultContent:(BOOL)arg23 editURL:(id)arg24 paidContent:(BOOL)arg25 privateContent:(BOOL)arg26 videoPro:(id)arg27 liveEventURL:(id)arg28 currentViewers:(unsigned long long)arg29 license:(id)arg30 keywords:(id)arg31 {
if(Ignition && YTAllowComments){ 
return %orig(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, TRUE, TRUE, arg21, arg22, FALSE, arg24, FALSE, FALSE, arg27, arg28, arg29, arg30, arg31);
} else{ return %orig; }}

-(BOOL)isCommentsAllowed { %log;  if (Ignition && YTAllowComments){ BOOL  r = FALSE; NSLog(@" isCommentsAllowed :  = %hhd", r); return r; }
else{ return %orig; } }
%end
 /****************************************************
// Feature: YTNoAds
/****************************************************/
%hook YTChannel
-(BOOL)paidContent { %log;  if (Ignition && YTNoAds){ BOOL  r = FALSE; NSLog(@" isMonetized :  = %hhd", r); return r; }
else{ return %orig; } }
%end

%hook YTIPlayerResponse
-(BOOL)isMonetized { %log;  if (Ignition && YTNoAds){ 
BOOL  r = FALSE; NSLog(@" isMonetized :  = %hhd", r); 
return r; }
else{ 
return %orig; 
} }
%end

%hook YTVideoAdsController
+(BOOL)isAdSenseAdTag:(id)arg1 { %log;  if (Ignition && YTNoAds){ BOOL  r = FALSE; NSLog(@" isAdSenseAdTag :  = %hhd", r); return r; }
else{ return %orig(arg1); } }

-(id)videoAdsService { %log;  if (Ignition && YTNoAds){ id  r = NULL; NSLog(@" isAdultContentConfirmed :  = %hhd", r); return NULL; }
else{ return %orig; } }
%end

%hook YTPlayerViewController
-(BOOL)isPlayingAd { %log;  if (Ignition && YTNoAds){ BOOL  r = FALSE; NSLog(@" isMonetized :  = %hhd", r); return r; }
else{ return %orig; } }
%end

%hook YTDataUtils
+(id)identifierForAdvertising { %log;  if (Ignition && YTNoAds){ id  r = NULL; NSLog(@" identifierForAdvertising :  = %hhd", r); return NULL; }
else{ return %orig; }  }
%end

%hook YTVideoAdsController
-(id)init { %log;  if (Ignition && YTNoAds){ id  r = NULL; NSLog(@" init :  = %hhd", r); return NULL; }
else{ return %orig; }  }

-(BOOL)isAdSenseAdTag:(id)arg1 { %log;  if (Ignition && YTNoAds){ BOOL  r = FALSE; id arg1 = NULL; NSLog(@" isMonetized :  = %hhd", r); %orig(arg1); return r; }
else{ return %orig; } }
%end

%hook YTVASTAd
-(BOOL)isForecastingAd { %log;  if (Ignition && YTNoAds){ BOOL  r = FALSE; NSLog(@" isForecastingAd :  = %hhd", r); return r; }
else{ return %orig; }  }

 /****************************************************
// Feature: YTAdSkipping
/****************************************************/
// -(BOOL)isSkippable { %log;  if (Ignition && YTAdSkipping){ BOOL  r = TRUE; NSLog(@" isInBackground :  = %hhd", r); return r; }
// else{ return %orig; } }
%end

// %hook YTVASTParser
// -(id)VASTAdWithLinearElement:(id)arg1 extensionsElement:(id)arg2 impressionURLs:(id)arg3 errorPingURLs:(id)arg4 adIDs:(id)arg5 adSystems:(id)arg6 adTagURL:(id)arg7 canBeSkippable:(BOOL)arg8 error:(id*)arg9 {
// if (Ignition && YTAdSkipping){ %orig(arg1, arg2, arg3, arg4, arg5, arg6, arg7, TRUE, arg9); }
// else{ return %orig; } }
// %end



/*
=======================
END OF METHOD HOOKS
=======================
*/
static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) { // Action when listener detects a change
    //Perform action when switching a preference specifier
	
    NSLog(@"Killing YouTube");
    system("killall YouTube"); //Close YouTube
    NSLog(@"Killing Settings.app");
    system("killall Settings"); //Close Settings

    
 NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //ceate pointer to peferences path
 
 Ignition = [preferences[PREFERENCES_ENABLED_SKPIgnition_KEY] boolValue];// This switch defines Ignition as the preference boolean
 //Ignition points to the Activate switch in the settings bundle

    YTHighQuality = [preferences[PREFERENCES_ENABLED_YTHighQuality_KEY] boolValue];// Switch Defeniton on initialization
    YTAdSkipping = [preferences[PREFERENCES_ENABLED_YTAdSkipping_KEY] boolValue];// Switch Defeniton on initialization
    YTBackgroundAudio = [preferences[PREFERENCES_ENABLED_YTBackgroundAudio_KEY] boolValue];// Switch Defeniton on initialization
    YTNoAgeLimit = [preferences[PREFERENCES_ENABLED_YTNoAgeLimit_KEY] boolValue];// Switch Defeniton on initialization
    YTNoAds = [preferences[PREFERENCES_ENABLED_YTNoAds_KEY] boolValue];// Switch Defeniton on initialization
    YTRemovePrivateContent = [preferences[PREFERENCES_ENABLED_YTRemovePrivateContent_KEY] boolValue];// Switch Defeniton on initialization
    YTAllowComments = [preferences[PREFERENCES_ENABLED_YTAllowComments_KEY] boolValue];// Switch Defeniton on initialization

  
}

%ctor { // runs first. This is what happens once the target (net.whatsapp.WhatsApp) is run. Prior to code-insertion. 

        if (![[NSFileManager defaultManager] fileExistsAtPath:@"/var/lib/dpkg/info/org.thebigboss.StreamPlay.list"]){
      otherRepo = YES;
    }
	
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) { //add listener

	NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //Define preference path here. (PREFERENCES_PATH).
	
	if (preferences == nil) {
	    preferences = @{ 
		    //PREFERENCES_ENABLED_SKPFeatureOne_KEY : @(NO), Don't forget commmas 
		    PREFERENCES_ENABLED_YTHighQuality_KEY : @(NO),
		    PREFERENCES_ENABLED_YTAdSkipping_KEY : @(NO),
		    PREFERENCES_ENABLED_YTBackgroundAudio_KEY : @(NO),		    
			PREFERENCES_ENABLED_YTNoAgeLimit_KEY : @(NO),
		    PREFERENCES_ENABLED_YTNoAds_KEY : @(NO),
		    PREFERENCES_ENABLED_YTRemovePrivateContent_KEY : @(NO),
		    PREFERENCES_ENABLED_YTAllowComments_KEY : @(NO),

		    PREFERENCES_ENABLED_SKPIgnition_KEY : @(NO)  // No comma

			
			
			}; // Default preference value to no
	    
	    [preferences writeToFile:PREFERENCES_PATH atomically:YES];
	} else {
 YTHighQuality = YES;	
 YTAdSkipping = YES;	
 YTBackgroundAudio = YES;	 
 YTNoAgeLimit = YES;	
 YTNoAds = YES;	
 YTRemovePrivateContent = YES;	
 YTAllowComments = YES;	
 
 Ignition = YES;
 
    YTHighQuality = [preferences[PREFERENCES_ENABLED_YTHighQuality_KEY] boolValue];// Switch Defeniton on initialization
    YTAdSkipping = [preferences[PREFERENCES_ENABLED_YTAdSkipping_KEY] boolValue];// Switch Defeniton on initialization
    YTBackgroundAudio = [preferences[PREFERENCES_ENABLED_YTBackgroundAudio_KEY] boolValue];// Switch Defeniton on initialization
    YTNoAgeLimit = [preferences[PREFERENCES_ENABLED_YTNoAgeLimit_KEY] boolValue];// Switch Defeniton on initialization
    YTNoAds = [preferences[PREFERENCES_ENABLED_YTNoAds_KEY] boolValue];// Switch Defeniton on initialization
    YTRemovePrivateContent = [preferences[PREFERENCES_ENABLED_YTRemovePrivateContent_KEY] boolValue];// Switch Defeniton on initialization
    YTAllowComments = [preferences[PREFERENCES_ENABLED_YTAllowComments_KEY] boolValue];// Switch Defeniton on initialization

 Ignition = [preferences[PREFERENCES_ENABLED_SKPIgnition_KEY] boolValue];
	    
		    //Checks if 'Activate' switch is on.
			if(Ignition){ 
	NSLog(@"StreamPlay: StreamPlay Activated: Initializing");
	// * you can 
	// Check if YTHighQuality switch is activated, too.
	if(YTHighQuality){NSLog(@"StreamPlay: YTHighQuality Activated"); 
	
	} else{NSLog(@"StreamPlay: YTHighQuality Deactivated"); }
	
		// If 'YTAdSkipping' switch is activated.
	if(YTAdSkipping){NSLog(@"StreamPlay: YTAdSkipping Activated"); 
	
	} else{NSLog(@"StreamPlay: YTAdSkipping Deactivated"); }
	
	// If  YTBackgroundAudio switch is activated
	if(YTBackgroundAudio){NSLog(@"StreamPlay: YTBackgroundAudio Activated"); 

	} else{NSLog(@"StreamPlay: YTBackgroundAudio Deactivated"); }	
	
	// If  YTNoAgeLimit switch is activated
	if(YTNoAgeLimit){NSLog(@"StreamPlay: YTNoAgeLimit Activated"); 
	
	} else{NSLog(@"StreamPlay: YTNoAgeLimit Deactivated"); }	

	// If  YTNoAds switch is activated
	if(YTNoAds){NSLog(@"StreamPlay: YTNoAds Activated"); 
	
	} else{NSLog(@"StreamPlay: YTNoAds Deactivated"); }

		// If  YTRemovePrivateContent switch is activated
	if(YTRemovePrivateContent){NSLog(@"StreamPlay: YTRemovePrivateContent Activated"); 
	
	} else{NSLog(@"StreamPlay: YTRemovePrivateContent Deactivated"); }

			// If  YTAllowComments switch is activated
	if(YTAllowComments){NSLog(@"StreamPlay: YTAllowComments Activated"); 
	
	} else{NSLog(@"StreamPlay: YTAllowComments Deactivated"); }

	}
	else{ NSLog(@"StreamPlay: StreamPlay Deactivated. Target will run unmodified.");
	}
	
	}
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, PreferencesChangedCallback, CFSTR(PREFERENCES_CHANGED_NOTIFICATION), NULL, CFNotificationSuspensionBehaviorCoalesce);
    }];
}
