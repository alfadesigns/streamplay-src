![](http://image-store.slidesharecdn.com/1b5007ff-abbb-449f-8e57-79394077e4c3-large.png)
## StreamPlay - Source Code Sample
-----------------------------

StreamPlay: Enhances the functionality of YouTube app for iOS.

###The Idea: 
- Look at the app as one giant switchboard. 
- Observe what happens when you turn some switches on, some off.
- Create a feature set based on the results. (Settings pane in Settings.app will allow users to switch on/off

Features included for the following apps:
- YouTube.app

### A product of reverse engineering on iOS By: Suhaib Alfageeh
### Language: Objective-C & Logos preprocessor directives. 

- 1) Download YouTube 
- 2) Dump the binary 
- 3) Analyze assembly 
- 4) Study code-base from dumped binary headers included in YouTube binary 
- 5) Inject own logic into hooked classes.
